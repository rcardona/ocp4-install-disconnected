## OCP4 Installer Provided Infrastructure Disconnected Installation

#### 0 - Prerequisites

The installation program requires access to port 9440 on Prism Central and Prism Element, more detailed information [HERE](https://docs.openshift.com/container-platform/4.13/installing/installing_nutanix/installing-restricted-networks-nutanix-installer-provisioned.html#prerequisites).

  - To confirme that port 9440 is accessible. Control plane nodes must be able to reach Prism Central and Prism Element on port 9440 for the installation to succeed.

  - If your Nutanix environment is using the default self-signed SSL/TLS certificate, replace it with a certificate that is signed by a CA. The installation program requires a valid CA-signed certificate to access to the Prism Central API. For more information about replacing the self-signed certificate, see the [Nutanix AOS Security Guide](https://portal.nutanix.com/page/documents/details?targetId=Nutanix-Security-Guide-v6_1:mul-security-ssl-certificate-pc-t.html).

  - Create DNS records for the two static IP addresses in the appropriate DNS server. These must be of the form
    - __api.<cluster_name>.<base_domain>__ , for the API VIP
    - __*.apps.<cluster_name>.<base_domain>__ , for the ingress VIP

  - Getting artifacts
    - Download and extract the `oc ` desire version, it will be used to extract the `openshift-install` binary in following steps
      ```bash
      $ wget https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/openshift-client-linux.tar.gz
      ```
    - Download the `Pull Secret` file from https://console.redhat.com/ and save it to a fiel called `~/pull-secret.txt`
      - Ex.
      ```text
      $ cat ~/pull-secret.txt
      ```
      
      ```json
      {"auths":{"cloud.openshift.com":{"auth":"b3BlbnNo ...  oUEZCa2JSbw==","email":"john@doe.com"}}}
      ```

#### 1 - Deploying mirrored registry
  - create mirrored registry procedure [HERE](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/cluster-registry/mirror-registry-commons.md?ref_type=heads).

  - Make sure that the `imageContentSources` section is recorded after the images mirroring

  - Make sure that a customized `openshift-install` binary specifically for the mirrored registry

#### 2 - Cloud Credential Operator utility
  - obtain the OpenShift release image
    ```bash
    $ RELEASE_IMAGE=$(./openshift-install version | awk '/release image/ {print $3}')
    ```

  - get the CCO container image from the release image and it executable
    ```bash
    $ CCO_IMAGE=$(oc adm release info --image-for='cloud-credential-operator' $RELEASE_IMAGE)
    ```

  - extract the ccoctl binary from the CCO container image and make it executable
    ```bash
    $ oc image extract $CCO_IMAGE --file="/usr/bin/ccoctl" -a pull_secret.json
    ```

  - create a directory called `creds`, and place file `pc_credentials.yaml` in it
    ```text
    $ cat creds/pc_credentials.yaml
    ```

    ```yaml
    credentials:
    - type: basic_auth
      data:
        prismCentral:
          username: <username_for_prism_central>
          password: <password_for_prism_central>
    ```

  - extract the `CredentialsRequest` objects for Nutanix Cloud Platform from the release image and store it in a directory called “credreqs”
    ```bash
    $ oc adm release extract --credentials-requests --cloud=nutanix \
      --to=credreqs -a pull_secret.json $RELEASE_IMAGE
    ```
 
  - use the ccoctl tool to process the CredentialsRequest objects and generate the secret manifests, which will be required later.
    ```bash
    $ ccoctl nutanix create-shared-secrets --credentials-requests-dir=credreqs \
      --output-dir=. --credentials-source-filepath=creds/pc_credentials.yaml
    ```

    ```text
    Output:
    2023/09/24 04:01:58 Saved credentials configuration to: manifests/openshift-machine-api-nutanix-credentials-credentials.yaml
    ```

  - verify that the file has been created; the expected output should be as seen below
    ```bash
    $ cat manifests/openshift-machine-api-nutanix-credentials-credentials.yaml
    ```

    ```text
    apiVersion: v1
    kind: Secret
    metadata:
      name: nutanix-credentials
      namespace: openshift-machine-api
    type: Opaque
    data:
      credentials: **************************************************************************
    ```

#### 3 - Create installation definition file
  - create `install-config` file
  ```bash
  $ openshift-install create install-config
  ```

  ```text
  ? SSH Public Key /root/.ssh/id_rsa.pub
  ? Platform nutanix
  ? Prism Central myprisma.johndoe.nl
  ? Port 9440
  ? Username demo-admin
  ? Password [? for help] ************
  INFO Connecting to Prism Central myprisma.johndoe.nl 
  ? Prism Element TY-UYP033
  ? Subnet Secondary-demo
  ? Virtual IP Address for API 10.10.10.11
  ? Virtual IP Address for Ingress 10.10.10.12
  ? Base Domain johndoe.nl
  ? Cluster Name ocp4
  ? Pull Secret [? for help] ************************************************************************
  ***************************************************************************************************
  INFO Install-Config created in: .  
  ```

  - check the information in `install-config.yaml`, replace accordingly, and $`\textcolor{red}{\text{BACK UP THIS FILE BEFORE USING IT}}`$
    - Ex.
    ```bash
    $ cat install-config.yaml
    ```

    ```yaml
    apiVersion: v1
    baseDomain: johndoe.nl
    compute:
    - architecture: amd64
      hyperthreading: Enabled
      name: worker
      platform: {}
      replicas: 3
    controlPlane:
      architecture: amd64
      hyperthreading: Enabled
      name: master
      platform: {}
      replicas: 3
    credentialsMode: Manual
    metadata:
      creationTimestamp: null
      name: ocp4
    networking:
      clusterNetwork:
      - cidr: 10.128.0.0/14
        hostPrefix: 23
      machineNetwork:
      - cidr: 10.0.0.0/16
      networkType: OpenShiftSDN
      serviceNetwork:
      - 172.30.0.0/16
    platform:
      nutanix:
        apiVIP: 10.10.10.11
        ingressVIP: 10.10.10.12
        prismCentral:
          endpoint:
            address: myprisma.johndoe.nl
            port: 9440
          password: ********
          username: demo-admin
        prismElements:
        - endpoint:
            address: 10.10.10.9
            port: 9440
          uuid: 0005e4c8-1f34-9dc5-0000-000000014039
        subnetUUIDs:
        - 8346214e-584c-4689-b525-c6019bbc4856
    publish: External
    pullSecret: '{"auths": …}'
    sshKey: '********'
    ```

    - create the manifests
    ```bash
    $ openshift-install create manifests
    ```
----

#### 4 - Adding Nutanix root CA certificates
  - commands, ex [HERE](https://portal.nutanix.com/page/documents/details?targetId=API-Ref-AOS-v6_7:dat-api-getCACertificates-auto-r.html)
  ```bash
  cp certs/lin/* /etc/pki/ca-trust/source/anchors
  ```
  
  ```bash
  update-ca-trust extract
  ```


---

### FROM HERE DOWN IT IS STILL IN CONSTRUCTION

### 0.2 - Downloading the RHCOS cluster image

Obtain the OpenShift Container Platform installation program and the pull secret for your cluster. For a restricted network installation, these files are on your mirror host.

```bash
openshift-install coreos print-stream-json
```

### 1.0 - Create the install-config.yaml file

```bash
openshift-install create install-config --dir <installation_directory> 
```

### 1.1 - Deploying the cluster

```bash
openshift-install create cluster --dir <installation_directory> --log-level=info 
```

### 2.0 - Example of ImageContentSource in install-config.yaml
```yaml
imageContentSources: 
- mirrors:
  - <local_registry>/<local_repository_name>/release
  source: quay.io/openshift-release-dev/ocp-release
- mirrors:
  - <local_registry>/<local_repository_name>/release
  source: quay.io/openshift-release-dev/ocp-v4.0-art-dev
```

- example
```yaml
imageContentSources:
- mirrors:
  - kvm.my.ka:5000/ocp4/openshift4
  source: quay.io/openshift-release-dev/ocp-release
- mirrors:
  - kvm.my.ka:5000/ocp4/openshift4
  source: quay.io/openshift-release-dev/ocp-v4.0-art-dev
```

- example for additionalBundle certificate
```yaml
additionalTrustBundle: | 
  -----BEGIN CERTIFICATE-----
  ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
  -----END CERTIFICATE-----
```
- adding custer image location
```yaml
    subnetUUIDs:
    - c7938dc6-7659-453e-a688-e26020c68e43
    clusterOSImage: http://example.com/images/rhcos-47.83.202103221318-0-nutanix.x86_64.qcow2 
```

>>>>





>>>>









### EXTRA

- This part of the mirroring corresponds to the creation of the installation program that is based on the content that were mirrored in the previous step.
```bash
oc adm release extract -a ${LOCAL_SECRET_JSON} --command=openshift-install "${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}:${OCP_RELEASE}-${ARCHITECTURE}"
```
